//
//  Article.swift
//  GenesysTest
//
//  Created by Nisso Pardo on 13/03/2022.
//

import Foundation

struct Image: Codable {
    var url: String
    var type: String
    var expression: String
    var width: String
    var height: String
    var description: String
}

struct Article: Codable {
    var title: String
    var link: String
    var description: String
    var author: String
    var pubDate: String
    var category: [String]
    var image: Image
}
