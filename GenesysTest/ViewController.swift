//
//  ViewController.swift
//  GenesysTest
//
//  Created by Nisso Pardo on 10/03/2022.
//

import UIKit

class ViewController: UITableViewController {
    var articles: [Article]?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        Fetcher.fetch(request: FeedRequest("https://bold360ai-mobile-artifacts.s3.amazonaws.com/interview-json/feed.json")) {
            switch $0 {
            case .success(let result):
                self.articles = result
                self.tableView.reloadData()
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
                
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ArticleTableViewCell
        cell.article = articles?[indexPath.row]
        return cell
    }

}

