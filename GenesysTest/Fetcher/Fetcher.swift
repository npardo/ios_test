//
//  Fetcher.swift
//  GenesysTest
//
//  Created by Nisso Pardo on 10/03/2022.
//

import Foundation

class Fetcher {
    static func fetch<T>(request: GBaseRequest<T>,
                         completion: @escaping ((Result<T?, Error>) -> ())) {
        
        if let url = request.url {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                guard let data = data else {
                    DispatchQueue.main.async {
                        completion(.failure(error!))
                    }
                    return
                }
                let temp = request.parser(data)
                DispatchQueue.main.async {
                    completion(.success(temp))
                }
                
            }.resume()
        } else {
            completion(.failure(NSError(domain: "fetch", code: 10001, userInfo: nil)))
        }
    }
}
