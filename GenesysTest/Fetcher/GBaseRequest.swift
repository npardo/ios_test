//
//  GBaseRequest.swift
//  GenesysTest
//
//  Created by Nisso Pardo on 13/03/2022.
//

import UIKit

class GBaseRequest<T>: NSObject {
    
    let url: URL?
    
    init(_ url: URL?) {
        self.url = url
    }
    
    init(_ string: String?) {
        self.url = URL(string: string ?? "")
    }

    var parser: ((Data) -> T?)! {
        return {
            return $0 as? T
        }
    }
}

class FeedRequest: GBaseRequest<[Article]> {
    override var parser: ((Data) -> [Article]?)! {
        return {
            let decoder = JSONDecoder()
            do {
               let array = try decoder.decode([Article].self, from: $0)
               return array
            } catch let error {
                debugPrint(error)
            }
            return nil
        }
    }
}

