//
//  ArticleTableViewCell.swift
//  GenesysTest
//
//  Created by Nisso Pardo on 13/03/2022.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
    
    
    
    var article: Article? {
        didSet {
            textLabel?.text = article?.title
            detailTextLabel?.text = article?.description
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }



}
